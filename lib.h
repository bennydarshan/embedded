struct block{
	char hash[66];
	unsigned int height;
	unsigned int total;
	char time[21];
	char relayed[22];
	char prev[66];

};

void printdb();
void buildb(struct block *out);
void print_block(struct block *iter);
void print_by_hash(struct block *self, char *hash);
void print_by_height(struct block *self, int height);
void export(struct block *self);
