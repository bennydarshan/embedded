#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct block{
	char hash[66];
	unsigned int height;
	unsigned int total;
	char time[21];
	char relayed[22];
	char prev[66];

};

void printdb()
{
	FILE *fp;
	char buff[1024];
	fp = fopen("blockdb", "r");
	while(fgets(buff, 1024, fp)) printf("%s", buff);

}
void buildb(struct block *out)
{
	FILE *fp;
	char buff[1024];
	fp = fopen("blockdb", "r");
	for(struct block *iter = out; iter<out + 10; ++iter) {
		//printf("Got before here\n");
		fscanf(fp, "hash: %s\nheight: %d\ntotal: %d\ntime: %s\nrelayed_by: %s\nprev_block: %s\n", iter->hash, &(iter->height), &(iter->total), iter->time, iter->relayed, iter->prev);
		
	}
}

void print_block(struct block *iter)
{
	printf("hash: %s\nheight: %d\ntotal: %d\ntime: %s\nrelayed_by: %s\nprev_block: %s\n", iter->hash, iter->height, iter->total, iter->time, iter->relayed, iter->prev);
}

void print_by_hash(struct block *self, char *hash)
{
	for(struct block *iter = self; iter<self + 10; ++iter) {
		if(strcmp(iter->hash, hash) == 0) {
			print_block(iter);
			return;
		}
	}
	printf("Not found!\n");
}

void print_by_height(struct block *self, int height)
{
	for(struct block *iter = self; iter<self + 10; ++iter) {
		if(iter->height == height) {
			print_block(iter);
			return;
		}
	}
	printf("Not found!\n");
}

void export(struct block *self)
{
	FILE *fp = fopen("export.csv", "w");
	fprintf(fp,"hash,height,total,time,relayed_by,prev_block\n");
	for(struct block *iter = self; iter<self + 10; ++iter) {
		fprintf(fp, "%s,%d,%d,%s,%s,%s\n", iter->hash, iter->height, iter->total, iter->time, iter->relayed, iter->prev);
	}
}
