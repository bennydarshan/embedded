all: main.c lib.so lib.h
	c99 main.c lib.so -o blocker

lib.so: lib.c
	c99 -shared -fPIC -o lib.so lib.c

clean: 
	rm -rf lib.so blocker
