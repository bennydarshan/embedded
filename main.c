#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "lib.h"

int main(int argc, char *argv[])
{
	if(argc == 1) {
		printdb();
		return 0;
	}
	if(strcmp("--help",argv[1]) == 0) {
		printf("Print usage\n");
		return 0;
	}
	struct block blocks[10];
	buildb(blocks);
	if(strcmp("--hash", argv[1]) == 0) {
		print_by_hash(blocks, argv[2]);
		return 0;
	}
	if(strcmp("--height", argv[1]) == 0) {
		print_by_height(blocks, atoi(argv[2]));
		return 0;
	}
	if(strcmp("--export", argv[1]) == 0) {
		export(blocks);
		return 0;
	}
	if(strcmp("--download", argv[1]) == 0) {
		system("./download.sh");
		return 0;
	}
	return 0;
}
